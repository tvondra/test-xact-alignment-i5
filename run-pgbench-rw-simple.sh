#!/bin/sh

p=$PATH
WARMUP=600
DURATION=300
SCALES="50 300"
CLIENTS="4 8 16"
THREADS=4
RUNS=5
NAME='pgbench-rw-simple'

# load shared functions
. ./functions.sh

function warmup_cluster
{
	d=$1
	s=$2

	mkdir -p "$d/$s/warmup"

	stats_collector_start "$d/$s/warmup"

	pgbench -c 32 -j $THREADS -T $WARMUP -l --aggregate-interval=1 pgbench > $d/$s/warmup/pgbench.log 2>&1

	stats_collector_stop

	mv pgbench_log.* $d/$s/warmup
}

function run_benchmark
{
	d=$1
	s=$2
	c=$3
	r=$4

	mkdir -p "$d/$s/benchmark/$c/$r"

	psql -c checkpoint postgres > /dev/null 2>&1

	stats_collector_start "$d/$s/benchmark/$c/$r"

	pgbench -c $c -j $THREADS -T $DURATION -l --aggregate-interval=1 pgbench > $d/$s/benchmark/$c/$r/pgbench.log 2>&1

	stats_collector_stop

	mv pgbench_log.* $d/$s/benchmark/$c/$r
}

PATH=/home/postgres/pg-master/bin:$p

for s in $SCALES; do

	echo [`date`] "$NAME master scale=$s init"

	init_cluster $NAME/master $s ''

	echo [`date`] "$NAME master scale=$s warmup"

	warmup_cluster $NAME/master $s

	for r in `seq 1 $RUNS`; do

		for c in $CLIENTS; do

			echo [`date`] "$NAME master scale=$s clients=$c runs=$r benchmark"

			run_benchmark $NAME/master $s $c $r

		done

	done

	echo [`date`] "$NAME master scale=$s done"

	stop_cluster

	git add $NAME/master/$s > /dev/null 2>&1
	git commit -m "$NAME/master/$s" > /dev/null 2>&1
	git push > /dev/null 2>&1

done


PATH=/home/postgres/pg-xact/bin:$p

for s in $SCALES; do

        echo [`date`] "$NAME xact scale=$s init"

        init_cluster $NAME/xact $s ''

        echo [`date`] "$NAME xact scale=$s warmup"

        warmup_cluster $NAME/xact $s

        for r in `seq 1 $RUNS`; do

                for c in $CLIENTS; do

                        echo [`date`] "$NAME xact scale=$s clients=$c runs=$r benchmark"

                        run_benchmark $NAME/xact $s $c $r

                done

        done

        echo [`date`] "$NAME xact scale=$s done"

        stop_cluster

        git add $NAME/xact/$s > /dev/null 2>&1
        git commit -m "$NAME/xact/$s" > /dev/null 2>&1
        git push > /dev/null 2>&1

done

